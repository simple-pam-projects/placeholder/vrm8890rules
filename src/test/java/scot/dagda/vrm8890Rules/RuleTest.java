/*
 * Copyright 2015 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package scot.dagda.vrm8890Rules;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.drools.core.time.SessionPseudoClock;
import org.junit.Test;
import org.kie.api.KieBase;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.definition.KiePackage;
import org.kie.api.definition.rule.Rule;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scot.dagda.vrm8890DataModel.Audit;
import scot.dagda.vrm8890DataModel.Lease;
import scot.dagda.vrm8890DataModel.SalesRecommendation;
import scot.dagda.vrm8890DataModel.Vehicle;

public class RuleTest {

  static final Logger LOG = LoggerFactory.getLogger(RuleTest.class);

  @Test
  public void test() {
    KieServices kieServices = KieServices.Factory.get();

    KieContainer kContainer = kieServices.getKieClasspathContainer();
    Results verifyResults = kContainer.verify();
    for (Message m : verifyResults.getMessages()) {
      LOG.info("{}", m);
    }

    LOG.info("Creating kieBase");
    KieBase kieBase = kContainer.getKieBase();

    LOG.info("There should be rules: ");
    for (KiePackage kp : kieBase.getKiePackages()) {
      for (Rule rule : kp.getRules()) {
        LOG.info("kp " + kp + " rule " + rule.getName());
      }
    }

    LOG.info("Creating kieSession");
    KieSession session = kieBase.newKieSession();

    LOG.info("Now running data");

    {
      Vehicle v = new Vehicle();
      v.setVehicleId("v1car");
      v.setVehicleType("CAR");
      session.insert(v);
    }
    {
      Vehicle v = new Vehicle();
      v.setVehicleId("v2wav");
      v.setVehicleType("WAV");
      session.insert(v);
    }
    {
      Vehicle v = new Vehicle();
      v.setVehicleId("v3lcv");
      v.setVehicleType("LCV");
      session.insert(v);
    }
    {
      Vehicle v = new Vehicle();
      v.setVehicleId("v1car_complex_1");
      v.setVehicleType("CAR");
      v.setCondition("good");
      session.insert(v);
    }
    {
      Lease l = new Lease();
      l.setVehicleId("v1car_complex_1");
      l.setExpiryDate(LocalDate.of(2020, 9, 01));
      session.insert(l);
    }
    {
      Vehicle v = new Vehicle();
      v.setVehicleId("v1car_complex_2");
      v.setVehicleType("CAR");
      v.setCondition("bad");
      session.insert(v);
    }
    {
      Lease l = new Lease();
      l.setVehicleId("v1car_complex_2");
      l.setExpiryDate(LocalDate.of(2020, 9, 01));
      session.insert(l);
    }
    session.fireAllRules();

    LOG.info("Final checks");

    // assertEquals("Size of object in Working Memory is 15", 15, session.getObjects().size());

    // ---
    {
      LOG.info("---");
      LOG.info("Check Sales Recommendation for v1car");
      boolean salesRecommendation = false;
      String v1Platform = "mfldirect";
      String v1Channel = "Pre-Expiry";
      String vid = "v1car";
      Iterator<? extends Object> oIter = session.getObjects().iterator();
      while (oIter.hasNext()) {
        Object o = oIter.next();
        if (o instanceof SalesRecommendation) {
          SalesRecommendation s = (SalesRecommendation) o;
          if (s.getVehicleId().equalsIgnoreCase(vid)) {
            salesRecommendation = true;
            LOG.info(s.toString());
            assertEquals("Sales Recommendation for v1car platfom is " + v1Platform, v1Platform, s.getSalesPlatform());
            assertEquals("Sales Recommendation for v1car channel is " + v1Channel, v1Channel, s.getSalesChannel());
          }
        }
      }
      assertEquals("Sales Recommendation for v1car found", true, salesRecommendation);
    }

    // ---
    {
      LOG.info("Check Audit for v1car");
      boolean audit = false;
      String vid = "v1car";
      Iterator<? extends Object> oIter = session.getObjects().iterator();
      while (oIter.hasNext()) {
        Object o = oIter.next();
        if (o instanceof Audit) {
          Audit a = (Audit) o;
          if (a.getVehicleId().equalsIgnoreCase(vid)) {
            audit = true;
            LOG.info(a.toString());
          }
        }
      }
      assertEquals("Audit for v1car found", true, audit);
    }

    // ---
    {
      LOG.info("---");
      LOG.info("Check Sales Recommendation for v2wav");
      boolean salesRecommendation = false;
      String v1Platform = "N/A";
      String v1Channel = "N/A";
      String vid = "v2wav";
      Iterator<? extends Object> oIter = session.getObjects().iterator();
      while (oIter.hasNext()) {
        Object o = oIter.next();
        if (o instanceof SalesRecommendation) {
          SalesRecommendation s = (SalesRecommendation) o;
          if (s.getVehicleId().equalsIgnoreCase(vid)) {
            salesRecommendation = true;
            LOG.info(s.toString());
            assertEquals("Sales Recommendation for v2wav platfom is " + v1Platform, v1Platform, s.getSalesPlatform());
            assertEquals("Sales Recommendation for v2wav channel is " + v1Channel, v1Channel, s.getSalesChannel());
          }
        }
      }
      assertEquals("Sales Recommendation for v2wav found", true, salesRecommendation);
    }

    // ---
    {
      LOG.info("Check Audit for v2wav");
      boolean audit = false;
      String vid = "v2wav";
      Iterator<? extends Object> oIter = session.getObjects().iterator();
      while (oIter.hasNext()) {
        Object o = oIter.next();
        if (o instanceof Audit) {
          Audit a = (Audit) o;
          if (a.getVehicleId().equalsIgnoreCase(vid)) {
            audit = true;
            LOG.info(a.toString());
          }
        }
      }
      assertEquals("Audit for v2wav found", true, audit);
    }

    // ---
    {
      LOG.info("---");
      LOG.info("Check Sales Recommendation for v3lcv");
      boolean salesRecommendation = false;
      String v1Platform = "N/A";
      String v1Channel = "N/A";
      String vid = "v3lcv";
      Iterator<? extends Object> oIter = session.getObjects().iterator();
      while (oIter.hasNext()) {
        Object o = oIter.next();
        if (o instanceof SalesRecommendation) {
          SalesRecommendation s = (SalesRecommendation) o;
          if (s.getVehicleId().equalsIgnoreCase(vid)) {
            salesRecommendation = true;
            LOG.info(s.toString());
            assertEquals("Sales Recommendation for v3lcv platfom is " + v1Platform, v1Platform, s.getSalesPlatform());
            assertEquals("Sales Recommendation for v3lcv channel is " + v1Channel, v1Channel, s.getSalesChannel());
          }
        }
      }
      assertEquals("Sales Recommendation for v3lcv found", true, salesRecommendation);
    }

    // ---
    {
      LOG.info("Check Audit for v3lcv");
      boolean audit = false;
      String vid = "v3lcv";
      Iterator<? extends Object> oIter = session.getObjects().iterator();
      while (oIter.hasNext()) {
        Object o = oIter.next();
        if (o instanceof Audit) {
          Audit a = (Audit) o;
          if (a.getVehicleId().equalsIgnoreCase(vid)) {
            audit = true;
            LOG.info(a.toString());
          }
        }
      }
      assertEquals("Audit for v3lcv found", true, audit);
    }

    // ---
    {
      LOG.info("---");
      LOG.info("Check Sales Recommendation for v1car_complex_1");
      boolean salesRecommendation = false;
      String v1Platform = "mfldirect";
      String v1Channel = "Pre-Expiry";
      String vid = "v1car_complex_1";
      Iterator<? extends Object> oIter = session.getObjects().iterator();
      while (oIter.hasNext()) {
        Object o = oIter.next();
        if (o instanceof SalesRecommendation) {
          SalesRecommendation s = (SalesRecommendation) o;
          if (s.getVehicleId().equalsIgnoreCase(vid)) {
            salesRecommendation = true;
            LOG.info(s.toString());
            assertEquals("Sales Recommendation for v1car_complex_1 platfom is " + v1Platform, v1Platform, s.getSalesPlatform());
            assertEquals("Sales Recommendation for v1car_complex_1 channel is " + v1Channel, v1Channel, s.getSalesChannel());
          }
        }
      }
      assertEquals("Sales Recommendation for v1car_complex_1 found", true, salesRecommendation);
    }
    {
      LOG.info("Check Audit for v1car_complex_1");
      boolean audit = false;
      String vid = "v1car_complex_1";
      Iterator<? extends Object> oIter = session.getObjects().iterator();
      while (oIter.hasNext()) {
        Object o = oIter.next();
        if (o instanceof Audit) {
          Audit a = (Audit) o;
          if (a.getVehicleId().equalsIgnoreCase(vid)) {
            audit = true;
            LOG.info(a.toString());
          }
        }
      }
      assertEquals("Audit for v1car_complex_1 found", true, audit);
    }

    // ---
    {
      LOG.info("---");
      LOG.info("Check Sales Recommendation for v1car_complex_2");
      boolean salesRecommendation = false;
      String v1Platform = "mfldirect";
      String v1Channel = "Pre-Expiry";
      String vid = "v1car_complex_2";
      Iterator<? extends Object> oIter = session.getObjects().iterator();
      while (oIter.hasNext()) {
        Object o = oIter.next();
        if (o instanceof SalesRecommendation) {
          SalesRecommendation s = (SalesRecommendation) o;
          if (s.getVehicleId().equalsIgnoreCase(vid)) {
            salesRecommendation = true;
            LOG.info(s.toString());
            assertEquals("Sales Recommendation for v1car_complex_2 platfom is " + v1Platform, v1Platform, s.getSalesPlatform());
            assertEquals("Sales Recommendation for v1car_complex_2 channel is " + v1Channel, v1Channel, s.getSalesChannel());
          }
        }
      }
      assertEquals("Sales Recommendation for v1car_complex_2 found", true, salesRecommendation);
    }
    {
      LOG.info("Check Audit for v1car_complex_2");
      boolean audit = false;
      String vid = "v1car_complex_2";
      Iterator<? extends Object> oIter = session.getObjects().iterator();
      while (oIter.hasNext()) {
        Object o = oIter.next();
        if (o instanceof Audit) {
          Audit a = (Audit) o;
          if (a.getVehicleId().equalsIgnoreCase(vid)) {
            audit = true;
            LOG.info(a.toString());
          }
        }
      }
      assertEquals("Audit for v1car_complex_2 found", true, audit);
    }


  }
}
